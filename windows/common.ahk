; Terminal
^!t::Run powershell.exe

; Toggle desktops
^!Left::SendInput {Ctrl down}{LWin down}{Left}{LWin up}{Ctrl up}
^!Right::SendInput {Ctrl down}{LWin down}{Right}{LWin up}{Ctrl up}
