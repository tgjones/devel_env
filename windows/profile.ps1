﻿# Function that operate on environment variables
. "$PSScriptRoot\environment.ps1"

# Anaconda environment wrappers
$condaRoot = "$Env:USERPROFILE\Anaconda3"
function Conda-Activate([string]$condaEnv) {Invoke-CmdScript $condaRoot\Scripts\activate.bat $condaEnv}
function Conda-Deactivate {Invoke-CmdScript $condaRoot\Scripts\deactivate.bat}

# Missing *nix style aliases
Set-Alias ll Get-ChildItem
Set-Alias which Get-Command
Set-Alias vi vim
