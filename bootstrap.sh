#!/bin/bash

# Spacemacs
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

# Oh-My-Zsh
git clone https://github.com/robbyrussell/oh-my-zsh ~/.oh-my-zsh

# Pyenv
git clone https://github.com/yyuu/pyenv.git ~/.pyenv
git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv

# Vim plugins (Pathogen)
git clone https://github.com/tpope/vim-sensible.git ~/.vim/bundle/vim-sensible
git clone https://github.com/mitsuhiko/vim-jinja.git ~/.vim/bundle/vim-jinja
git clone --recursive https://github.com/davidhalter/jedi-vim.git ~/.vim/bundle/jedi-vim
git clone https://github.com/hashivim/vim-terraform.git ~/.vim/bundle/vim-terraform
