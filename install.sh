#!/bin/bash

usage ()
{
    echo "Setup script for dotfiles..."
    echo " Usage: $0 [-f -l -d <dir>]"
    echo
    echo "  -h       : Display this message"
    echo "  -f       : Force overwrite of existing files without backing them up"
    echo "  -l       : Symlink files instead of copying"
    echo "  -b       : Run 'bootstrap.sh' before installing dotfiles"
    echo "  -d <dir> : Specify directory to install to instead of $HOME"
}

# Option defaults
force=false
link=false
install_dir=""
dotfile_dir="$PWD/dotfiles"

# Parse command line options
while getopts "hflbnd:" opt
do
    case $opt in
        h)
            usage
            exit 1
            ;;
        f)
            force=true
            ;;
        l)
            link=true
            ;;
        b)
            bootstrap=true
            ;;
        d)
            install_dir=$OPTARG
            ;;
    esac
done

# Ensure dotfiles submodule is cloned
if [[ ! -f $dotfile_dir/README.md ]]; then
    git submodule update --init
fi

# Run bootstrap
if [[ "$bootstrap" == true ]]; then
    echo "INFO: Executing 'bootstrap.sh'"
    eval $PWD/bootstrap.sh
fi

# Files to install
ignore_regex="README.md|.git\/"
files=$(find "$dotfile_dir" -type f | egrep -v "$ignore_regex")

# Get home directory from environment or command line
if [[ "$install_dir" == "" ]]; then
    if [[ "$HOME" == "" ]]; then
        echo "ERRO: \$HOME is not set, please specify install directory with -d parameter."
        exit
    else
        install_dir="$HOME"
    fi
fi

echo "INFO: Installing config files to directory '$install_dir'..."

# Backup/remove existing files
for file in $files
do
    if [[ -e $install_dir/$file ]]; then
        if [[ "$force" == true ]]; then
            rm -Rf $install_dir/$file
        else
            mv $install_dir/$file $install_dir/$file.$$
            echo "INFO: Existing $file found, backed up to $install_dir/$file.$$"
        fi
    fi
done

# Copy/symlink files
for file in $files
do
    file_dir=$(dirname $file | sed "s|^$dotfile_dir||")

    mkdir -p "$install_dir"/"$file_dir"

    if [[ "$link" == true ]]; then
        ln -sf "$file" "$install_dir"/"$file_dir"
        echo "INFO: Symbolic link to '$file' created"
    else
        cp -Rf "$file" "$install_dir"/"$file_dir"
        echo "INFO: Installed '$file'"
    fi
done
