Development Environment
=======================

Building
--------

    docker build -t tgjones/devel_env:latest .

Running
-------

    docker run -d -h devel -p 3122:22 -e DISPLAY=tornado:0.0 -v c:/Users/tgjon/Workspace:/root/workspace --name devel tgjones/devel_env:latest

Pushing
-------

    docker login
    docker push tgjones/devel_env:latest

SSH Config (~/.ssh/config)
--------------------------

    Host devel
      User root
      Port 3122
      HostName localhost
      ForwardX11 yes
      ForwardX11Trusted yes

SSH
---

    ssh devel

Copy Public Key
---------------

    cat ~/.ssh/id_rsa.pub | ssh devel 'cat >> .ssh/authorized_keys'
    secret