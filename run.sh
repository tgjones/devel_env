#!/bin/bash

if [[ $(uname -s) == CYGWIN* ]]; then
    docker run --name devel -h devel -d -p 3122:22 \
        -e DISPLAY=$(hostname):0.0 \
        -v $(cygpath -w $HOME/Workspace):/root/workspace \
        -v pyenv-versions:/root/.pyenv/versions \
        devel_env:latest
else
    xsock=/tmp/.X11-unix
    xauth=/tmp/.docker.xauth
    xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -q -f $xauth nmerge -
    docker run --name devel -h devel -d -p 3122:22 \
            -e DISPLAY=$DISPLAY \
            -e XAUTHORITY=$xauth \
            -v /tmp/.X11-unix:/tmp/.X11-unix \
            -v $HOME/workspace:/root/workspace \
            -v pyenv-versions:/root/.pyenv/versions \
            devel_env:latest
fi

