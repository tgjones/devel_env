FROM ubuntu:16.10
MAINTAINER Tim Jones <tgjonesuk@gmail.com>

RUN apt-get update && apt-get upgrade -y 
RUN apt-get install -y build-essential language-pack-en openssh-server git zsh \
    tmux direnv vim emacs libgl1-mesa-glx

RUN echo root:secret | chpasswd
RUN chsh -s /usr/bin/zsh
RUN mkdir /var/run/sshd
RUN touch /root/.Xauthority
RUN echo PermitUserEnvironment yes >> /etc/ssh/sshd_config
RUN sed -i 's/^PermitRootLogin.*$/PermitRootLogin yes/' /etc/ssh/sshd_config

COPY bootstrap.sh /tmp
RUN /tmp/bootstrap.sh
COPY dotfiles /root

ENV DISPLAY=:0.0
ENV LANG=en_GB.UTF-8
ENV LC_ALL=en_GB.UTF-8

VOLUME /root/workspace

CMD env > ~/.ssh/environment && /usr/sbin/sshd -D

EXPOSE 22
